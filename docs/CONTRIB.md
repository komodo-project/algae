# Contributing Guidelines 🤝

If you want to improve Algae, here is how you can contribute:

1. Create a **new branch** and name it accordingly depending on your change. For example, the industry standard is: username/change..
2. Commit to your branch and make changes as necessary.
3. When ready, submit a merge request. It will be reviewed - make sure to provide enough details about the changes!

Now here are some rules that you have to follow:

1. **DO NOT** push to the master branch. This branch is only for code that works, and is the code intended for distribution. Make your changes according to the guide.
2. **DO** explain your changes in details. Use detailed commits - bullet points are your friends!