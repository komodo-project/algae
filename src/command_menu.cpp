// Implementation of commands, links to further functions

#include "command_menu.h"
#include "part_reader.h"
#include "part_remover.h"
#include "part_creator.h"

#include <iostream>
#include <vector>
#include <string>

void renderMenu() {
    std::cout << "[ALGAE-CMD] ";
    std::string command;
    std::cin >> command;

    if (command == "partget") {
        std::vector<PartitionInfo> partitions = getSystemPartitions();

        // Display the information
        for (const auto &partition: partitions) {
            std::cout << "Partition: " << partition.name << "\n";
            std::cout << "Mount Point: " << partition.mountPoint << "\n";
            std::cout << "Total Capacity: " << partition.totalCapacity << " KB\n";
            std::cout << "Free Space: " << partition.freeSpace << " KB\n\n";
        }

        renderMenu();
    }
    else if (command == "exit") {
        std::cout << "[ALGAE] Exiting..." << std::endl;
        exit(0);
    }
    else if (command == "partcreate") {
        std::string name;
        std::string mountPoint;
        int totalCapacity;
        int freeSpace;

        std::cout << "[ALGAE] Enter partition name: ";
        std::cin >> name;
        std::cout << "[ALGAE] Enter partition mount point: ";
        std::cin >> mountPoint;
        std::cout << "[ALGAE] Enter partition total capacity: ";
        std::cin >> totalCapacity;

        createPartition(name, mountPoint, totalCapacity, freeSpace);
        renderMenu();
    }

    else if (command == "partrm") {
        std::string name;
        std::cout << "[ALGAE] Enter partition name: ";
        std::cin >> name;

        removePartition(name);
        renderMenu();
    }

    else if (command == "help") {
        std::cout << "[ALGAE] List of commands:\n";
        std::cout << "partget - list partitions\npartcreate - create new partition\npartrm - remove partition\nexit - quit program\n";
        renderMenu();
    }
    
    else {
        std::cout << "[ALGAE] Invalid command." << std::endl;
        renderMenu();
    }
}
