#ifndef PART_CREATOR_H
#define PART_CREATOR_H

#include <string>

void createPartition(const std::string& name, const std::string& mountPoint, int totalCapacity, int freeSpace);

#endif // PART_CREATOR_H
