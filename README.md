# Algae

Algae is a simple command-line partitions manager with similar functionality to gpart.
You can view your partitions, remove them or create new ones.
Basically all the utilities you need. 
Algae uses C++20 and the CMake build system.

# TEMPORARY INSTALL

Download the repository, and position the "algae" file from build/ into /bin/ to use the algae command.
Don't worry, we will ship a package later. So far there were big problems with dpkg though...
