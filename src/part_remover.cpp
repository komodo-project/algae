// part_remover.cpp

#include "part_remover.h"
#include <iostream>
#include <cstdlib> // For system function


// Remove a partition
void removePartition(const std::string& partitionName) {

    std::cout << "[ALGAE] Removing partition: " << partitionName << std::endl;
    std::string removeCommand = "rm -r " + partitionName;

    // Use system command to remove the partition
    int result = system(removeCommand.c_str());

    if (result == 0) {
        std::cout << "[ALGAE] Partition removed successfully." << std::endl;
    } else {
        std::cerr << "[ALGAE] Error removing partition." << std::endl;
    }
}
