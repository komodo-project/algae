#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <sys/statvfs.h>

#include "part_reader.h"

std::vector<PartitionInfo> getSystemPartitions() {
    std::vector<PartitionInfo> partitions;

    std::ifstream mountsFile("/proc/mounts");
    if (!mountsFile.is_open()) {
        std::cerr << "[ALGAE] Error opening /proc/mounts\n";
        return partitions;
    }

    std::string line;
    while (std::getline(mountsFile, line)) {
        PartitionInfo partition;
        std::istringstream iss(line);

        // Extract relevant information from the line
        iss >> partition.name >> partition.mountPoint;

        // Skip non-disk partitions (e.g., tmpfs, proc)
        if (partition.name.find("/dev/") != std::string::npos) {
            // Statvfs is used to get disk space information
            struct statvfs stat;
            if (statvfs(partition.mountPoint.c_str(), &stat) == 0) {
                partition.totalCapacity = stat.f_blocks * stat.f_frsize / 1024; // convert to kilobytes
                partition.freeSpace = stat.f_bfree * stat.f_frsize / 1024;       // convert to kilobytes

                partitions.push_back(partition);
            } else {
                std::cerr << "[ALGAE] Error getting disk space information for " << partition.mountPoint << "\n";
            }
        }
    }

    return partitions;
}
