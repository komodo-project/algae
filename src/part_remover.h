#ifndef PART_REMOVER_H
#define PART_REMOVER_H

#include <string>

void removePartition(const std::string& partitionName);

#endif // PART_REMOVER_H
