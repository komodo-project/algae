// part_creator.cpp

#include "part_creator.h"
#include <vector>
#include "part_reader.h"
#include "part_creator.h"
#include <iostream>
#include <string>

std::vector<PartitionInfo> partitions;

void createPartition(const std::string& name, const std::string& mountPoint, int totalCapacity, int freeSpace) {
    PartitionInfo partition;
    partition.name = name;
    partition.mountPoint = mountPoint;
    partition.totalCapacity = totalCapacity;
    partition.freeSpace = freeSpace;

    partitions.push_back(partition);
    std::cout << "[ALGAE] Partition created." << std::endl;
}
