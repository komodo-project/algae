// part_reader.h

#ifndef PART_READER_H
#define PART_READER_H

#include <string>
#include <vector>

struct PartitionInfo {
    std::string name;
    std::string mountPoint;
    int totalCapacity;
    int freeSpace;
};

std::vector<PartitionInfo> getSystemPartitions();

#endif // PART_READER_H
